#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
	//DECLARATION
	srand(time(0));
	system("CLS");
	int hpHero, hpEnemy, minHD, maxHD, minOD, maxOD, commandHero, Effect;

	//USER STATUS
	cout << "Initializing Hero... Commence!" << endl;
	cout << "Input HP (Nickki): ";
	cin >> hpHero;
	cout << "Input Minimum Damage: ";
	cin >> minHD;
	cout << "Input Maximum Damage. Value must be Greater than or Equal to Minimum: ";
	cin >> maxHD;
	system("CLS"); // CLEARED SCREEN AFTER ENTERING YOUR STATUS

	//ENEMY STATUS
	cout << "Initializing Opponent... Commence!" << endl;
	cout << "Input HP (Enemy): ";
	cin >> hpEnemy;
	cout << "Input Minimum Damage: ";
	cin >> minOD;
	cout << "Input Maximum Damage. Value must be Greater than or Equal to Minimum: ";
	cin >> maxOD;
	system("CLS"); // CLEAR SCREEN AFTER INPUTING OPPONENT STATS
	cout << "Hero:" << hpHero << endl;
	cout << "Opponent:" << hpEnemy;
	cout << endl;

	//Battle Commands :)
	//Game Mechanics~
	cout << "GAME START!";
	cout << "" << endl;
	while (hpEnemy > 1 && hpEnemy > 1) {
		//RANDOM DAMAGE GENERATOR
		int heroDmg = (rand() % (maxHD - minHD + 1)) + minHD;
		int enemyDmg = (rand() % (maxOD - minOD + 1)) + minOD;
		int commandEnemy = (rand() % 3) + 1;
		cout << "" << endl;
		cout << "[1]ATTACK" << endl;
		cout << "[2]DEFENSE" << endl;
		cout << "[3]WILD ATTACK" << endl;
		cout << "ENTER YOUR COMMAND: ";
		cin >> commandHero;
		system("CLS");
		
		//GAME CONDITIONS AND RULES.
		if (commandHero == 1 && commandEnemy == 1){
			cout << "ENEMY ATTACKS!";
			cout << endl;
			hpHero = hpHero - enemyDmg;
			hpEnemy = hpEnemy - heroDmg;
			cout << "ENEMY DEALT " << enemyDmg << " WHILE YOU DEALT " << heroDmg << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 1 && commandEnemy == 2) {
			cout << "ENEMY DEFENDS!";
			cout << endl;
			hpEnemy = hpEnemy - (heroDmg / 2);
			cout << "YOUR ATTACK HAS BEEN HALVED DEALING " << heroDmg / 2 << " DAMAGE!" << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 1 && commandEnemy == 3) {
			cout << "ENEMY USED WILD ATTACK!";
			cout << endl;
			hpHero = hpHero - (enemyDmg * 2);
			hpEnemy = hpEnemy - heroDmg;
			cout << "ENEMY CRITICAL STRIKES DEALING " << enemyDmg * 2 << " DAMAGE" << " WHILE YOU DEALT " << heroDmg << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 2 && commandEnemy == 1) {
			cout << "ENEMY ATTACKS!";
			cout << endl;
			hpHero = hpHero - (enemyDmg / 2);
			cout << "ENEMY DAMAGE HAS BEEN CUT IN HALF! DEALING " << enemyDmg / 2 << " BECAUSE YOU CHOSE TO DEFEND! " << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 2 && commandEnemy == 2) {
			cout << "ENEMY DEFENDS!";
			cout << endl;
			cout << "Nothing Happens!" << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 2 && commandEnemy == 3) {
			cout << "ENEMY USED WILD ATTACK!";
			cout << endl;
			hpEnemy = hpEnemy - (heroDmg * 2);
			cout << "PLAYER SUCCESSFULLY DEFENDS AND COUNTERS THE OPPONENT'S ATTACK!" << endl;
			cout << "ENEMY DEALT " << enemyDmg << " WHILE YOU COUNTER HITS THE ENEMY DEALING " << heroDmg * 2 << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 3 && commandEnemy == 1) {
			cout << "ENEMY ATTACKS!";
			cout << endl;
			hpEnemy = hpEnemy - (heroDmg * 2);
			hpHero = hpHero - (enemyDmg * 2);
			cout << "ENEMY DEALT " << enemyDmg << " WHILE YOU CRITICALLY HITS THE ENEMY DEALING " << heroDmg << " DAMAGE" << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 3 && commandEnemy == 2) {
			cout << "ENEMY SUCCESSFULLY DEFEND AGAINST YOUR ATTACK!";
			cout << "Enemy counters your attack" << endl;
			cout << endl;
			hpHero = hpHero - (enemyDmg * 2);
			cout << "ENEMY COUNTER HITS DEALING " << enemyDmg << " DAMAGE " << ", WHILE YOU DEALT NOTHING" << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else if (commandHero == 3 && commandEnemy == 3) {
			cout << "ENEMY USED WILD ATTACK!";
			cout << endl;
			hpEnemy = hpEnemy - (heroDmg * 2);
			hpHero = hpHero - (enemyDmg * 2);
			cout << "ENEMY CRITICALLY HITS! DEALING " << enemyDmg  << " DAMAGE " << " WHILE YOU CRITICALLY HITS! DEALING " << heroDmg << " DAMAGE " << endl;
			cout << "Your Remaining HP: " << hpHero << endl;
			cout << "Enemy's Remaining HP: " << hpEnemy << endl;
		}
		else {
			cout << "INVALID OUTPUT!";
		}
		if (hpHero > 1 && hpEnemy < 1) {
			cout << "YOU WIN!";
		}
		if (hpEnemy > 1 && hpHero < 1) {
			cout << "YOU LOSE!";
		}

	}

	system("pause");
	return 0;
}