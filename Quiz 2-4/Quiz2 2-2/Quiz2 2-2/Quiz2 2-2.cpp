#include<iostream>

int main() {
	using namespace std;

	float Input1, Input2, Input3, Input4, Input5;
	float Average;

	//User Input
	cout << "Enter Input 1: ";
	cin >> Input1;
	cout << "Enter Input 2: ";
	cin >> Input2;
	cout << "Enter Input 3: ";
	cin >> Input3;
	cout << "Enter Input 4: ";
	cin >> Input4;
	cout << "Enter Input 5: ";
	cin >> Input5;

	//Formula
	Average = (Input1 + Input2 + Input3 + Input4 + Input5) / 5;

	cout << "=================" << endl;
	cout << "Average is " << Average << "%" << endl;

	system("Pause");
	return 0;
}