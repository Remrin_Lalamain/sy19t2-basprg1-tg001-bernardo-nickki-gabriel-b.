#include<iostream>

int main() {
	using namespace std;

	float radius;
	int Area, Circumference;
	const int pi = 3.14;

	cout << "Enter Radius: ";
	cin >> radius;
	
	Area = pi * (radius * radius);
	Circumference = 2 * pi * radius;

	cout << "Area = " << Area << endl;
	cout << "Circumference = " << Circumference << endl;


	system("Pause");
	return 0;
	
}