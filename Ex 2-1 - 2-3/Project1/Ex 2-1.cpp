#include<iostream>

int main() {

	using namespace std;
	int x, y;
	
	// User Input
	cout << "Enter Number for X: ";
	cin >> x;
	cout << "Enter Number for Y:";
	cin >> y;
	
	// SPACING
	cout << "==========================" << endl;
	cout << "==========================" << endl;
	cout << "==========================" << endl;
	cout << "" << endl;

	// Conditions for each Statements
	if (x == y) {
		cout << "B = 1" << endl;
	}
	else{
		cout << "B = 0" << endl;
	}
	if (x != y) {
		cout << "I = 1" << endl;
	}
	else {
		cout << "I = 0" << endl;
	}
	if (x > y) {
		cout << "N = 1" << endl;
	}
	else{
		cout << "N = 0" << endl;
	}
	if (x < y) {
		cout << "A = 1" << endl;
	}
	else {
		cout << "A = 0" << endl;
	}
	if (x >= y) {
		cout << "R = 1" << endl;
	}
	else {
		cout << "R = 0" << endl;
	}
	if (x <= y) {
		cout << "Y = 1" << endl;
	}
	else {
		cout << "Y = 0" << endl;
	}
	
	system("Pause");
	return 0;
}